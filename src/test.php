#!/usr/bin/php -q
<?php

echo "Start mysql testing connection\n";

$servername = getenv('MYSQL_HOST');
$username = getenv('MYSQL_USER');
$password = getenv('MYSQL_PASSWORD');
$dbname = getenv('MYSQL_DATABASE');

$dsn = "mysql:host=$servername;dbname=$dbname";

echo "Use dsn = $dsn\n";
echo "Use username = $username\n";
echo "Use password = $password\n\n";

try {
  $conn = new PDO($dsn, $username, $password);
  // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  echo "Connected successfully\n\n";
} catch(PDOException $e) {
  echo "Connection failed: " . $e->getMessage()."\n\n";
}
