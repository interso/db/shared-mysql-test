SHELL = /bin/bash
BASE_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
ROOT_DIR := $(BASE_DIR)/.make

include $(ROOT_DIR)/init.mk

start: ##@process Start
	@$(DC) up -d --force-recreate

stop: ##@process Stop
	@$(DC) stop

restart: stop start ##@process Restart all


test-local: ##@test Test application internal
	@$(DC) run --rm --no-deps --env LANG=C.UTF-8 app php src/test.php

test-external: start test-call stop ##@test Test application external

test-call:
	curl http://0.0.0.0:$(NGINX_PORT)/index.php

ps: ##@info Show status
	@$(DC) ps

logs: ##@info Show logs
	@$(DC) logs -f

db: ##@app Connect to mysql
	@docker run --env LANG=C.UTF-8 --rm -it --network $(EXTERNAL_NETWORK) imega/mysql-client mysql --host=$(MYSQL_HOST) --user=$(MYSQL_USER) --password=$(MYSQL_PASSWORD) $(MYSQL_DATABASE)


