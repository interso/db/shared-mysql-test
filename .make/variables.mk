DC := $(DOCKER_COMPOSE)
DC_EXEC := $(DC) exec
DC_RUN := $(DC) run

START := echo '...'
FINISH := echo 'OK'
